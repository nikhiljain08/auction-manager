import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, QuerySnapshot } from '@angular/fire/compat/firestore';
import { PropertySubType } from '../models/property-sub-type.model';

@Injectable({
  providedIn: 'root'
})
export class PropertySubTypeService {
  private dbPath = '/property_sub_type';

  propertySubTypeRef: AngularFirestoreCollection<PropertySubType>;

  constructor(private db: AngularFirestore) {
    this.propertySubTypeRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<PropertySubType> {
    return this.propertySubTypeRef;
  }

  get(type: String): Promise<QuerySnapshot<PropertySubType>> {
    console.log(type);
    return this.propertySubTypeRef.ref.where("type", "==", type).get();
  }
}
