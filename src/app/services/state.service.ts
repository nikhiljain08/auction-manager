import { Injectable } from '@angular/core';
import { State } from '../models/state.model';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private dbPath = '/state';

  stateRef: AngularFirestoreCollection<State>;

  constructor(private db: AngularFirestore) {
    this.stateRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<State> {
    return this.stateRef;
  }
}
