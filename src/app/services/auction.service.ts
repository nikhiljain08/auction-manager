import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/compat/firestore';
import { Auction } from '../models/auction.model';

@Injectable({
  providedIn: 'root'
})
export class AuctionService {
  private dbPath = '/auction';

  auctionRef: AngularFirestoreCollection<Auction>;

  constructor(private db: AngularFirestore) {
    this.auctionRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<Auction> {
    return this.auctionRef;
  }
}
