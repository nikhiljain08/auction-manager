import { Injectable } from '@angular/core';
import { PropertyType } from '../models/property-type.model';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class PropertyTypeService {
  private dbPath = '/property_type';

  propertyTypeRef: AngularFirestoreCollection<PropertyType>;

  constructor(private db: AngularFirestore) {
    this.propertyTypeRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<PropertyType> {
    return this.propertyTypeRef;
  }
}
