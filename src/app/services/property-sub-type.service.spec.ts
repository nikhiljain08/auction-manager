import { TestBed } from '@angular/core/testing';

import { PropertySubTypeService } from './property-sub-type.service';

describe('PropertySubTypeService', () => {
  let service: PropertySubTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PropertySubTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
