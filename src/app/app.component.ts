import { Component } from '@angular/core';
import { Auction } from './models/auction.model';
import { AuctionService } from './services/auction.service';
import { map } from 'rxjs';
import { State } from './models/state.model';
import { City } from './models/city.model';
import { District } from './models/district.model';
import { PropertyType } from './models/property-type.model';
import { PropertySubType } from './models/property-sub-type.model';
import { StateService } from './services/state.service';
import { PropertyTypeService } from './services/property-type.service';
import { PropertySubTypeService } from './services/property-sub-type.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'auction-manager';

  displayedColumns: String[] = ['position', 'name', 'weight', 'symbol'];
  dataSource: PeriodicElement[] = [];

  selectedAuction: String | undefined;
  selectedPropertyType: String | undefined;
  selectedPropertySubType: String | undefined;
  selectedState: String | undefined;
  selectedDistrict: String | undefined;
  selectedCity: String | undefined;
  selectedStartDate: String | undefined;
  selectedEndDate: String | undefined;

  auctionTypes: Auction[] = [];
  propertyTypes: PropertyType[] = [];
  propertySubTypes: PropertySubType[] = [];
  states: State[] = [];
  cities: City[] = [];
  districts: District[] = [];

  constructor(
    private auctionService: AuctionService,
    private propertyTypeService: PropertyTypeService,
    private propertySubTypeService: PropertySubTypeService,
    private stateService: StateService) { }

  ngOnInit(): void {
    this.retrieveAuctions();
    this.retrievePropertyTypes();
    this.retrieveStates();
  }

  retrieveAuctions(): void {
    this.auctionService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      data.sort((a, b) => (a?.name ?? '') < (b?.name ?? '') ? -1 : 1);
      this.auctionTypes = data;
    });
  }

  retrievePropertyTypes(): void {
    this.propertyTypeService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      data.sort((a, b) => (a?.name ?? '') < (b?.name ?? '') ? -1 : 1);
      this.propertyTypes = data;
    });
  }

  retrievePropertySubTypes(type: String): void {
    this.propertySubTypes = [];
    this.propertySubTypeService.get(type).then(data => {
      data.forEach(doc => {
        this.propertySubTypes.push(doc.data());
      });

      console.log(this.propertySubTypes);
      (this.propertySubTypes ?? []).sort((a, b) => (a?.name ?? '') < (b?.name ?? '') ? -1 : 1);
    });
  }

  retrieveStates(): void {
    this.stateService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      data.sort((a, b) => (a?.name ?? '') < (b?.name ?? '') ? -1 : 1);
      this.states = data;
    });
  }

  onPropertyChange() {
    this.retrievePropertySubTypes(this.selectedPropertyType ?? '');
  }

  addStartDateEvent(event: MatDatepickerInputEvent<Date>) {
    this.selectedStartDate = event.value?.toDateString();
  }

  addEndDateEvent(event: MatDatepickerInputEvent<Date>) {
    this.selectedEndDate = event.value?.toDateString();
  }

  search() {
    this.dataSource = ELEMENT_DATA;
    console.log(this.selectedStartDate);
    console.log(this.selectedEndDate);
  }

  reset() {
    this.dataSource = [];
  }
}
